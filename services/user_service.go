package services

import (
	"gitlab.com/mahdipardat21/bookstore_user-api/domain/users"
	"gitlab.com/mahdipardat21/bookstore_user-api/utils/crypto_utils"
	"gitlab.com/mahdipardat21/bookstore_user-api/utils/errors"
)

type UserService interface {
	CreateUser(users.User) (*users.User, *errors.RestErr)
	GetUserById(int64) (*users.User, *errors.RestErr)
	UpdateUser(bool, users.User) (*users.User, *errors.RestErr)
	DeleteUser(int64) *errors.RestErr
	SearchUser(string) (users.Users, *errors.RestErr)
}

type userService struct {}

func NewUserService() UserService {
	return &userService{}
}


func (s *userService) CreateUser(user users.User) (*users.User, *errors.RestErr) {
	if err := user.Validate(); err != nil {
		return nil, err
	}

	if err := user.Save(); err != nil {
		return nil, err
	}

	return &user, nil
}

func (s *userService) GetUserById(userId int64) (*users.User, *errors.RestErr) {
	var user *users.User = &users.User{Id: userId}

	if err := user.Get(); err != nil {
		return nil, err
	}

	return user, nil
}

func (s *userService) UpdateUser(isPartial bool, user users.User) (*users.User, *errors.RestErr) {
	current, err := s.GetUserById(user.Id)

	if err != nil {
		return nil, err
	}

	if isPartial {
		if user.FirstName != "" {
			current.FirstName = user.FirstName
		}

		if user.LastName != "" {
			current.LastName = user.LastName
		}

		if user.Email != "" {
			current.Email = user.Email
		}

		if user.Status != "" {
			current.Status = user.Status
		}

		if user.Password != "" {
			current.Password = crypto_utils.GetMD5(user.Password)
		}
	} else {
		current.FirstName = user.FirstName
		current.LastName = user.LastName
		current.Email = user.Email
		current.Status = user.Status
		current.Password = crypto_utils.GetMD5(user.Password)
	}

	err = current.Update()

	if err != nil {
		return nil, err
	}

	return current, nil
}

func (s *userService) DeleteUser(userId int64) *errors.RestErr {
	var user = users.User{Id: userId}
	err := user.Delete()

	if err != nil {
		return err
	}

	return nil
}

func (s *userService) SearchUser(status string) (users.Users, *errors.RestErr) {
	user := &users.User{}
	return user.FindBySearch(status)
}
