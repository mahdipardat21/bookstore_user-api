package main

import (
	"gitlab.com/mahdipardat21/bookstore_user-api/app"
	_ "gitlab.com/mahdipardat21/bookstore_user-api/datasources/postgres/users_db"
)



func main() {
	// run app
	app.StartApplication()
}