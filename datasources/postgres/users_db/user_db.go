package users_db

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/go-sql-driver/mysql"
)

var (
	Client *sql.DB
)

func init() {
	datasources := fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8", "root", "", "localhost:3306", "users_db")
	var err error
	Client, err = sql.Open("mysql", datasources)
	if err != nil {
		panic(err)
	}

	if err = Client.Ping(); err != nil {
		panic(err)
	}

	log.Println("postgres is running")
}
