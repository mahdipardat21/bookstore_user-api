module gitlab.com/mahdipardat21/bookstore_user-api

go 1.16

require (
	github.com/gin-gonic/gin v1.7.4
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	github.com/lib/pq v1.10.2 // indirect
)
