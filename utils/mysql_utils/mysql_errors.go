package mysql_utils

import (
	"strings"

	"github.com/go-sql-driver/mysql"
	"gitlab.com/mahdipardat21/bookstore_user-api/utils/errors"
)

const (
	noRowsQuery = "no rows in result set"
)

func ParseError(err error) *errors.RestErr {
	mysqlErr, ok := err.(*mysql.MySQLError)

	if !ok {
		if strings.Contains(err.Error(), noRowsQuery) {
			return errors.NewNotFound("row not found result")
		}

		return errors.NewInternalServerError("internal server error")
	}

	result := mysqlErrorHandle(mysqlErr)

	return result

}

func mysqlErrorHandle(err *mysql.MySQLError) *errors.RestErr {
	switch err.Number {
	case 1062:
		return errors.NewBadRequest("duplicate entry field")
	case 1065:
		return errors.NewBadRequest("empty query occured")

	}

	return errors.NewInternalServerError("internal mysql server error")
}
