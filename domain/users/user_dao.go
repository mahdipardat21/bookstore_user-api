package users

import (
	"fmt"

	"gitlab.com/mahdipardat21/bookstore_user-api/datasources/postgres/users_db"
	"gitlab.com/mahdipardat21/bookstore_user-api/utils/crypto_utils"
	"gitlab.com/mahdipardat21/bookstore_user-api/utils/date_utils"
	"gitlab.com/mahdipardat21/bookstore_user-api/utils/errors"
	"gitlab.com/mahdipardat21/bookstore_user-api/utils/mysql_utils"
)

const (
	queryInsertUser  = "INSERT INTO users(first_name, last_name, email, date_created, status, password) VALUES ( ?, ?, ?, ?, ?, ? );"
	queryFindOneUser = "SELECT id, first_name, last_name, email, date_created, status, password FROM users WHERE id=?;"
	queryUpdateUser  = "UPDATE users SET first_name=?, last_name=?, email=?, status=?, password=? WHERE id=?;"
	queryDeleteUser  = "DELETE FROM users WHERE id=?;"
	querySearchUser  = "SELECT id, first_name, last_name, email, date_created, status, password FROM users WHERE status=?;"
)

func (user *User) Save() *errors.RestErr {
	stmt, err := users_db.Client.Prepare(queryInsertUser)

	if err != nil {
		return errors.NewInternalServerError(fmt.Sprintf("Server Error: %s", err.Error()))
	}

	defer stmt.Close()

	user.DateCreated = date_utils.GetNowString()
	user.Password = crypto_utils.GetMD5(user.Password)

	insertRow, err := stmt.Exec(user.FirstName, user.LastName, user.Email, user.DateCreated, user.Status, user.Password)
	if err != nil {
		return mysql_utils.ParseError(err)
	}

	userId, err := insertRow.LastInsertId()

	if err != nil {
		return mysql_utils.ParseError(err)
	}

	user.Id = userId

	return nil
}

func (user *User) Get() *errors.RestErr {
	stmt, err := users_db.Client.Prepare(queryFindOneUser)

	if err != nil {
		return errors.NewInternalServerError(fmt.Sprintf("Server Error: %s", err.Error()))
	}

	defer stmt.Close()

	err = stmt.QueryRow(user.Id).Scan(&user.Id, &user.FirstName, &user.LastName, &user.Email, &user.DateCreated, &user.Status, &user.Password)

	if err != nil {
		return mysql_utils.ParseError(err)
	}

	return nil
}

func (user *User) Update() *errors.RestErr {
	stmt, err := users_db.Client.Prepare(queryUpdateUser)

	if err != nil {
		return errors.NewInternalServerError("internal server error")
	}

	defer stmt.Close()

	_, qErr := stmt.Exec(user.FirstName, user.LastName, user.Email, user.Status, user.Password, user.Id)

	if qErr != nil {
		return mysql_utils.ParseError(qErr)
	}

	return nil
}

func (user *User) Delete() *errors.RestErr {
	stmt, err := users_db.Client.Prepare(queryDeleteUser)

	if err != nil {
		return errors.NewInternalServerError("internal server error")
	}

	defer stmt.Close()

	_, qErr := stmt.Exec(user.Id)

	if qErr != nil {
		return mysql_utils.ParseError(qErr)
	}

	return nil
}

func (user *User) FindBySearch(status string) (Users, *errors.RestErr) {
	stmt, err := users_db.Client.Prepare(querySearchUser)

	if err != nil {
		return nil, errors.NewInternalServerError("internal server error")
	}

	defer stmt.Close()

	rows, err := stmt.Query(status)

	if err != nil {
		return nil, mysql_utils.ParseError(err)
	}

	defer rows.Close()

	results := make(Users, 0)
	for rows.Next() {
		var user User
		err := rows.Scan(&user.Id, &user.FirstName, &user.LastName, &user.Email, &user.DateCreated, &user.Status, &user.Password)

		if err != nil {
			return nil, mysql_utils.ParseError(err)
		}
		results = append(results, user)
	}

	return results, nil
}
