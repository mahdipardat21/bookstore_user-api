package users

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/mahdipardat21/bookstore_user-api/domain/users"
	"gitlab.com/mahdipardat21/bookstore_user-api/utils/errors"
	"gitlab.com/mahdipardat21/bookstore_user-api/services"
)

var (
	userService = services.NewUserService()
)

func CreateUser(c *gin.Context) {
	var user users.User

	// bytes, err := ioutil.ReadAll(c.Request.Body)

	// if err != nil {
	// 	// TODO: handle error
	// 	return
	// }

	// if err = json.Unmarshal(bytes, &user); err != nil {
	// 	// TODO: handle error
	// 	return
	// }

	if err := c.ShouldBindJSON(&user); err != nil {
		restErr := errors.NewBadRequest("invalid request body")
		c.JSON(restErr.Status, restErr)
		return
	}

	result, saveErr := userService.CreateUser(user)

	if saveErr != nil {
		c.JSON(saveErr.Status, saveErr)
		return
	}

	c.JSON(http.StatusCreated, result.Marshal(c.GetHeader("X-Public") == "true"))
}

func GetUser(c *gin.Context) {
	userId, err := strconv.ParseInt(c.Param("user_id"), 10, 64)

	if err != nil || userId <= 0 {
		cerr := errors.NewBadRequest("invalid user_id params")
		c.JSON(cerr.Status, cerr)
		return
	}

	result, userErr := userService.GetUserById(userId)

	if userErr != nil {
		c.JSON(userErr.Status, userErr)
		return
	}

	c.JSON(http.StatusOK, result.Marshal(c.GetHeader("X-Public") == "true"))

}

func UpdateUser(c *gin.Context) {
	userId, err := strconv.ParseInt(c.Param("user_id"), 10, 64)

	if err != nil || userId <= 0 {
		cerr := errors.NewBadRequest("invalid user_id params")
		c.JSON(cerr.Status, cerr)
		return
	}

	var user users.User
	user.Id = userId
	err = c.ShouldBindJSON(&user)

	if err != nil {
		cerr := errors.NewBadRequest("invalid json body")
		c.JSON(cerr.Status, cerr)
		return
	}

	isPartial := c.Request.Method == http.MethodPatch

	result, updateErr := userService.UpdateUser(isPartial, user)

	if updateErr != nil {
		c.JSON(updateErr.Status, updateErr)
		return
	}

	c.JSON(http.StatusOK, result.Marshal(c.GetHeader("X-Public") == "true"))
}

func DeleteUser(c *gin.Context) {
	userId, err := strconv.ParseInt(c.Param("user_id"), 10, 64)

	if err != nil {
		cerr := errors.NewBadRequest("invalid user_id params")
		c.JSON(cerr.Status, cerr)
		return
	}

	deleteErr := userService.DeleteUser(userId)

	if deleteErr != nil {
		c.JSON(deleteErr.Status, deleteErr)
		return
	}

	c.JSON(http.StatusOK, map[string]string{"status": "deleted"})
}

func SearchUser(c *gin.Context) {
	status := c.Query("status")

	if status == "" {
		status = "active"
	}

	users, err := userService.SearchUser(status)

	if err != nil {
		c.JSON(err.Status, err)
		return
	}

	c.JSON(http.StatusOK, users.Marshal(c.GetHeader("X-Public") == "true"))

}
