package app

import (
	"gitlab.com/mahdipardat21/bookstore_user-api/controllers/ping"
	"gitlab.com/mahdipardat21/bookstore_user-api/controllers/users"
)

func map_urls() {

	// ping pong
	router.GET("/ping", ping.Pong)

	// users
	router.GET("/users", users.SearchUser)
	router.POST("/users", users.CreateUser)
	router.GET("/users/:user_id", users.GetUser)
	router.PUT("/users/:user_id", users.UpdateUser)
	router.PATCH("/users/:user_id", users.UpdateUser)
	router.DELETE("/users/:user_id", users.DeleteUser)
}
