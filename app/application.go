package app

import (
	"fmt"

	"github.com/gin-gonic/gin"
)

var (
	router = gin.Default()
)

func StartApplication() {
	
	// routing
	map_urls()

	// run application
	router.Run(":3000")
	fmt.Println("Application running.")
}